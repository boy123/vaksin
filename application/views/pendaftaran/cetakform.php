<div class="row">
	<div class="col-md-12">
		<form action="" method="POST">
		<div class="form-group">
			<label>Jadwal Vaksin</label>
			<select class="form-control" name="id_jadwal" required="">
				<option>Pilih</option>
				<?php foreach ($this->db->get('jadwal_vaksin')->result() as $rw): ?>
					<option value="<?php echo $rw->id_jadwal ?>"><?php echo $rw->tanggal_vaksin.' - '.get_data('lokasi','id_lokasi',$rw->id_lokasi,'lokasi') ?></option>
				<?php endforeach ?>
				
			</select>
		</div>
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Cetak</button>
		</div>
		</form>
	</div>

</div>