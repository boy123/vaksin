<div class="row">
	<div class="col-md-12">

		<div style="margin: 10px;">
			<div class="row">

				<form action="app/aksi_import" method="POST" enctype="multipart/form-data">
					<div class="col-md-2">

						<input type="file" name="file_excel" class="form-control">
						<a href="files/template.xlsx" target="_blank" class="label label-success">Download Template</a><br>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-primary">Import</button>
					</div>

				</form>
			</div>
		</div>

		<table class="table table-bordered" id="example2">
			<thead>
				<tr>
					<th>No</th>
					<th>NIK</th>
					<th>Nama Lengkap</th>
					<th>Jenis Kelamin</th>
					<th>Tanggal Lahir</th>
					<th>Usia</th>
					<th>No HP</th>
					<th>Alamat Domisili</th>
					<th>Tgl & Lokasi</th>
					<th>Kode SESI</th>
					<th>No URUT</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$this->db->order_by('id_pendaftaran', 'desc');
				foreach ($this->db->get('pendaftaran')->result() as $rw): ?>
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->nik ?></td>
					<td><?php echo $rw->nama ?></td>
					<td><?php echo $rw->jenis_kelamin ?></td>
					<td><?php echo $rw->tanggal_lahir ?></td>
					<td><?php echo $rw->usia ?></td>
					<td><?php echo $rw->no_hp ?></td>
					<td><?php echo $rw->alamat ?></td>
					<td>
						<?php 
						$query = "
						SELECT a.id_lokasi, a.tanggal_vaksin
						FROM jadwal_vaksin a
						INNER JOIN sesi b ON a.id_jadwal=b.id_jadwal
						where b.kode_sesi = '$rw->kode_sesi'
						";

						$jadwal = $this->db->query($query);
						$lokasi = get_data('lokasi','id_lokasi',$jadwal->row()->id_lokasi,'lokasi');

						echo $jadwal->row()->tanggal_vaksin.' '.$lokasi
						 ?>
					</td>
					<td><?php echo $rw->kode_sesi ?></td>
					<td><?php echo $rw->no_urut ?></td>
				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
	</div>
</div>