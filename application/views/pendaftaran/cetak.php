<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Cetak_Peserta_Vaksin.xls");
?>
<style> .str{ mso-number-format:\@; } </style>

	<h3>Data Pendaftaran Vaksin Tanggal <?php echo get_data('jadwal_vaksin','id_jadwal',$id_jadwal,'tanggal_vaksin').' '.get_data('lokasi','id_lokasi',get_data('jadwal_vaksin','id_jadwal',$id_jadwal,'id_lokasi'),'lokasi') ?></h3>
	<table border="1">
		<tr>
			<td><b>No</b></td>
			<td><b>Kode Sesi</b></td>
			<td><b>Keterangan</b></td>
			<td><b>No Urut</b></td>
			<td><b>NIK</b></td>
			<td><b>Nama Lengkap</b></td>
			<td><b>Jenis Kelamin</b></td>
			<td><b>Tanggal Lahir</b></td>
			<td><b>Usia</b></td>
			<td><b>Alamat</b></td>
			<td><b>No HP</b></td>
		</tr>
		<?php 
		$no = 1;
		$this->db->where('id_jadwal', $id_jadwal);
		foreach ($this->db->get('sesi')->result() as $rw) {
			$this->db->where('kode_sesi', $rw->kode_sesi);
			foreach ($this->db->get('pendaftaran')->result() as $br): ?>
			<tr>
				<td><?php echo $no ?></td>
				<td><?php echo $rw->kode_sesi ?></td>
				<td><?php echo strip_tags($rw->keterangan)?></td>
				<td><?php echo $br->no_urut ?></td>
				<td class="str"><?php echo $br->nik ?></td>
				<td><?php echo $br->nama ?></td>
				<td><?php echo $br->jenis_kelamin ?></td>
				<td><?php echo $br->tanggal_lahir ?></td>
				<td><?php echo $br->usia ?></td>
				<td><?php echo $br->alamat ?></td>
				<td class="str"><?php echo $br->no_hp ?></td>
			</tr>
			<?php endforeach; $no++; }?>
	</table>
