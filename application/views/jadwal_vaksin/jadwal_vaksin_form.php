
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="date">Tanggal Vaksin <?php echo form_error('tanggal_vaksin') ?></label>
            <input type="date" class="form-control" name="tanggal_vaksin" id="tanggal_vaksin" placeholder="Tanggal Vaksin" value="<?php echo $tanggal_vaksin; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Lokasi <?php echo form_error('id_lokasi') ?></label>
            <select name="id_lokasi" class="form-control">
                <option value="<?php echo $id_lokasi ?>"><?php echo get_data('lokasi','id_lokasi',$id_lokasi,'lokasi') ?></option>
                <?php foreach ($this->db->get('lokasi')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_lokasi ?>"><?php echo $rw->lokasi ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="date">Dosis Dua <?php echo form_error('dosis_dua') ?></label>
            <input type="date" class="form-control" name="dosis_dua" id="dosis_dua" placeholder="Dosis Dua" value="<?php echo $dosis_dua; ?>" />
        </div>
	    <input type="hidden" name="id_jadwal" value="<?php echo $id_jadwal; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jadwal_vaksin') ?>" class="btn btn-default">Cancel</a>
	</form>
   