
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('jadwal_vaksin/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('jadwal_vaksin/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('jadwal_vaksin'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Tanggal Vaksin</th>
		<th>Lokasi</th>
        <th>Dosis Dua</th>
		<th>Status</th>
		<th>Action</th>
            </tr><?php
            foreach ($jadwal_vaksin_data as $jadwal_vaksin)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $jadwal_vaksin->tanggal_vaksin ?></td>
			<td><?php echo get_data('lokasi','id_lokasi',$jadwal_vaksin->id_lokasi,'lokasi') ?></td>
			<td><?php echo $jadwal_vaksin->dosis_dua ?></td>
            <td>
                <?php if ($jadwal_vaksin->aktif == 'y'): ?>
                    <span class="text-success">Aktif</span>
                <?php else: ?>
                    <span class="text-danger">Tidak Aktif</span>
                <?php endif ?>
            </td>
			<td style="text-align:center" width="200px">
                <?php if ($jadwal_vaksin->aktif == 'y'): ?>
                    <a href="jadwal_vaksin/set/<?php echo $jadwal_vaksin->id_jadwal ?>/t" class="label label-danger">Non Aktif</a>
                <?php else: ?>
                    <a href="jadwal_vaksin/set/<?php echo $jadwal_vaksin->id_jadwal ?>/y" class="label label-success">Aktif</a>
                <?php endif ?>
                

                <a href="sesi/index?id_jadwal=<?php echo $jadwal_vaksin->id_jadwal ?>&judul=<?php echo $jadwal_vaksin->tanggal_vaksin.' '.get_data('lokasi','id_lokasi',$jadwal_vaksin->id_lokasi,'lokasi') ?>" class="label label-default">Detail Sesi</a>
				<?php 
				echo anchor(site_url('jadwal_vaksin/update/'.$jadwal_vaksin->id_jadwal),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('jadwal_vaksin/delete/'.$jadwal_vaksin->id_jadwal),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    