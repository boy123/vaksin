
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('sesi/create').'?'.param_get(),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('sesi/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('sesi'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
        <th>Kode Sesi</th>
		<th>Jam Mulai</th>
		<th>Jam Selesai</th>
		<th>Kategori</th>
		<th>Kuota</th>
		<th>Keterangan</th>
		<th>Action</th>
            </tr><?php
            $id_jadwal = $this->input->get('id_jadwal');
            $sesi_data = $this->db->get_where('sesi', ['id_jadwal'=>$id_jadwal]);
            foreach ($sesi_data->result() as $sesi)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $sesi->kode_sesi ?></td>
			<td><?php echo $sesi->jam_mulai ?></td>
			<td><?php echo $sesi->jam_selesai ?></td>
			<td><?php echo $sesi->kategori ?></td>
			<td><?php echo $sesi->kuota ?></td>
			<td><?php echo $sesi->keterangan ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('sesi/update/'.$sesi->id_sesi).'?'.param_get(),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('sesi/delete/'.$sesi->id_sesi),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        </div>
       
    