
        <form action="<?php echo $action.'?'.param_get(); ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Kode Sesi <?php echo form_error('kode_sesi') ?></label>
            <input type="hidden" name="id_jadwal" value="<?php echo $this->input->get('id_jadwal') ?>">
            <input type="text" class="form-control" name="kode_sesi" id="kode_sesi" placeholder="Kode Sesi" value="<?php echo $kode_sesi; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Jam Mulai <?php echo form_error('jam_mulai') ?></label>
            <input type="time" class="form-control" name="jam_mulai" id="jam_mulai" placeholder="Jam Mulai" value="<?php echo $jam_mulai; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Jam Selesai <?php echo form_error('jam_selesai') ?></label>
            <input type="time" class="form-control" name="jam_selesai" id="jam_selesai" placeholder="Jam Selesai" value="<?php echo $jam_selesai; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Kategori <?php echo form_error('kategori') ?></label>
            <select class="form-control" name="kategori">
                <option value="<?php echo $kategori ?>"><?php echo get_data('kategori_peserta','kode',$kategori,'kategori') ?></option>
                <?php foreach ($this->db->get('kategori_peserta')->result() as $rw): ?>
                    <option value="<?php echo $rw->kode ?>"><?php echo $rw->kategori ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Kuota <?php echo form_error('kuota') ?></label>
            <input type="text" class="form-control" name="kuota" id="kuota" placeholder="Kuota" value="<?php echo $kuota; ?>" />
        </div>
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control textarea_editor" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_sesi" value="<?php echo $id_sesi; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('sesi').'?'.param_get() ?>" class="btn btn-default">Cancel</a>
	</form>
   