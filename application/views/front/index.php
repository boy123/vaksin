<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('front/head'); ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<body>

  <!-- ======= Header ======= -->
  <?php $this->load->view('front/header'); ?>

  <!-- ======= Hero Section ======= -->
  <div id="app">
  <section id="hero">

    <div class="container">
      <div class="col" data-aos="fade-right">
          <img src="front/header-logo1.png" class="img-fluid" alt="">
        </div>
      <div class="row">
        
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="fade-up">
          <div>
            <h1>POLTEKKES BERBAGI UNTUK NEGERI... CIPTAKAN HERD IMMUNITY</h1>
            <h2>Silahkan Klik Link dibawah ini untuk mendaftar atau Cek Jadwal Vaksin Anda</h2>
            <a href="#pendaftaran" class="btn btn-secondary">
              Pendaftaran Vaksin
            </a>
            <a href="#cekjadwal" class="btn btn-primary">
              Cek Jadwal Vaksin
            </a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
          <img src="front/vaksin-1.jpg" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->
  <section id="hero">

    <div class="container">

      <div class="row">
        
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="fade-up">
          <div>
            <h1>TATA TERTIB VAKSINASI</h1>
            <img src="front/check2.png" class="img-fluid" alt=""> Datang sesuai tanggal dan jam yang telah didaftarkan </br>
            <img src="front/check2.png" class="img-fluid" alt=""> Pastikan kondisi dalam Keadaan FIT, tidak sedang sakit </br>
            <img src="front/check2.png" class="img-fluid" alt=""> Jika ada penyakit kronis, membawa surat rekomendasi dari dokter</br>
            <img src="front/check2.png" class="img-fluid" alt=""> Sarapan/Makan terlebih dahulu sebelum divaksin</br>
            <img src="front/check2.png" class="img-fluid" alt=""> Membawa KTP / KK serta Fotokopi KTP / KK jika tidak ada KTP</br>
            <img src="front/check2.png" class="img-fluid" alt=""> Menggunakan baju yang mudah disingsingkan lengannya</br>
            <img src="front/check2.png" class="img-fluid" alt="">Menjalankan Protokol Kesehatan 5M
            
            
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
          <img src="front/5-M1.png" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->
  

  <main id="main">

    

    

    

    <!-- ======= Pricing Section ======= -->
    <section id="cekjadwal" class="pricing section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Cek Jadwal Vaksin</h2>
          <p>Isi NIK Anda untuk melihat jadwal vaksin</p>
          <br>
          
        </div>


        <div class="row">
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="name">Nomor Induk Kependudukan (NIK)</label>
              <input type="number" oninput="if(value.length>16)value=value.slice(0,16)"  class="form-control" name="nik" v-model="nik_jadwal"  placeholder="Masukkan NIK Anda" />
            </div>
            <div class="form-group">
              <button class="btn btn-primary" v-on:click="cariJadwal">Cek Jadwal Vaksin</button>
            </div>
          </div>

          <div class="col-md-12" v-if="cekNikJadwal">
            <table class="table table-bordered table-striped">
              <strong>
              <tr>
                <td><b>Nama Lengkap</b></td>
                <td>:</td>
                <td><a href="#" class="btn btn-success btn-md"><b>{{ dataNikJadwal.nama }}</b></a></td>
              </tr>
              <tr>
                <td><b>No. Urut Sesi</b></td>
                <td>:</td>
                <td><a href="#" class="btn btn-warning btn-md"><b>{{ dataNikJadwal.no_urut }}</b></a></td>
              </tr>
              <tr>
                <td><b>NIK</b></td>
                <td>:</td>
                <td><b>{{ dataNikJadwal.nik }}</b></td>
              </tr>
              <tr>
                <td><b>Jadwal Dosis 1</b></td>
                <td>:</td>
                <td><a href="#" class="btn btn-info btn-md"><b>{{ dataNikJadwal.jadwal }}</b></a></td>
              </tr>
              <tr>
                <td><b>Jadwal Dosis ke 2</b></td>
                <td>:</td>
                <td><a href="#" class="btn btn-primary btn-md"><b>{{ dataNikJadwal.dosis_dua }}</b></td>
              </tr>
            </table>
            
            <div class="alert alert-warning"><b>Sesi Vaksinasi</b></div>
            <div class="table-responsive">
                <table class="table table-bordered">
                  <tr>
                    <td>Jam Vaksin</td>
                    <td>Keterangan</td>
                  </tr>
                  <tr v-for="(data, index) in dataNikJadwal.data"
                :key="data.id_sesi">
                    <td><a href="#" class="btn btn-warning btn-lg"><b>{{ data.jam }}</b></a></td>
                    <td v-html="data.keterangan"></td>
                  </tr>
                      
                  
                  </strong>
                </table>
                
              </div>
                <div class="alert alert-danger">Harap datang sesuai Tanggal dan Jam yang tertera, jika tidak sesuai, mohon maaf maka tidak kami layani. Terima kasih</div>
          </div>
          
        </div>

      </div>
    </section><!-- End Pricing Section -->

    

    <!-- ======= Contact Section ======= -->
    <section id="pendaftaran" class="contact section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Pendaftaran Vaksin</h2>
        </div>

        <div class="row">

          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-left">
            <form action="web/simpan_pendaftaran" method="post" role="form" class="php-email-form">

              <div class="form-group">
                <label>Pilih Jadwal Vaksin</label>
                <select class="form-control" v-model="jadwal" v-on:change="cekJadwal" required>
                  <option>Pilih Jadwal Vaksin</option>
                  <?php foreach ($this->db->get_where('jadwal_vaksin', ['aktif'=>'y'])->result() as $rw): 
                    $sql = "SELECT sum(kuota) as total FROM sesi where id_jadwal='$rw->id_jadwal'";
                    $total_kuota = $this->db->query($sql)->row()->total;

                    $sql1 = "SELECT a.nik FROM pendaftaran a INNER JOIN sesi b ON a.kode_sesi=b.kode_sesi where b.id_jadwal='$rw->id_jadwal'";
                    $ttl = $this->db->query($sql1)->num_rows();

                    $sisa_kuota = $total_kuota - $ttl;
                  ?>
                    <option value="<?php echo $rw->id_jadwal ?>"><?php echo $rw->tanggal_vaksin.' |  '.get_data('lokasi','id_lokasi',$rw->id_lokasi,'lokasi').' | Kuota : '.$total_kuota.' | Sisa Kuota : '.$sisa_kuota ?></option>
                  <?php endforeach ?>
                </select>
                <input type="hidden" name="kode_sesi" v-model="kode_sesi">
              </div>

              <div v-if="pilih_jadwal">
                <p>Silahkan Pilih Sesi Vaksin</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped">
                  <tr>
                    <td>Pilih</ttd>
                    <td>Jam Vaksin</td>
                    <td>Sisa Kuota</td>
                    <td>Keterangan</td>
                    <td>Kategori</td>
                  </tr>
                  <tr v-for="(data, index) in sesiData"
                :key="data.id_sesi">
                    <td >
                      <input v-if="data.sisa_kuota > 0" type="radio" name="pilih" v-on:click="pilihSesi(data.kode_sesi)" required="">
                    </td>
                    <td>{{ data.jam }}</td>
                    <td>{{ data.sisa_kuota }}</td>
                    <td v-html="data.keterangan"></td>
                    <td>{{ data.kategori }}</td>
                    
                  </tr>
                </table>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">NIK</label>
                  <input type="text" name="nik" pattern="[0-9]+" class="form-control" minlength="16" maxlength="16" required />
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama" required/>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-4">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jenis_kelamin" required>
                    <option value="">Pilih</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name="tanggal_lahir" required/>
                </div>
                <div class="form-group col-md-4">
                  <label>No HP (AKTIF, Bisa Terima SMS)</label>
                  <input type="tel" pattern="[0-9]+" minlength="11" maxlength="13" class="form-control" name="no_hp" required/>
                </div>
              </div>

              <div class="form-group">
                <label>Alamat Domisili</label>
                <textarea class="form-control" minlength="10" name="alamat" rows="3" data-rule="required"></textarea>
              </div>

              
              
              <div class="text-center"><button type="submit">Daftar Vaksin</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->
  </div>


  <!-- Modal -->
  <div class="modal fade" id="mdlPaket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pemberitahuan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>
            Silahkan install aplikasi Point Pintar di playstore dan lakukan pendaftaran seperti biasa, pilih paket di menu berlangganan. <br>
          </p>
          <a href="https://play.google.com/store/apps/details?id=com.apps.pointpintar" target="_blank">
              <img src="front/img/download_plystore.png" style="height: 50px;">
            </a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          
        </div>
      </div>
    </div>
  </div>
  <!-- Tutup Modal -->


  <!-- ======= Footer ======= -->
  <?php $this->load->view('front/footer'); ?>
  
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script>
      var app = new Vue({
        el: "#app",
        data: {
          pilih_jadwal: false,
          jadwal: '',
          kode_sesi: '',
          sesiData : '',
          nik_jadwal: '',

          cekNikJadwal: false,
          dataNikJadwal: '',

          urlBase: '<?php echo base_url() ?>'
        },
        methods: {
          
           cekJadwal: function (){
              axios.get(this.urlBase + 'app/cekJadwal/'+this.jadwal)
                .then(res => {
                    this.sesiData = res.data;
                    this.pilih_jadwal = true
                })
                .catch(err => {
                    // handle error
                    console.log(err);
                })
           },

           pilihSesi: function (kodeSesi) {
             this.kode_sesi = kodeSesi
           },

           cariJadwal: function() {
              axios.get(this.urlBase + 'app/cekNikJadwal/'+this.nik_jadwal)
                .then(res => {
                    this.dataNikJadwal = res.data;
                    console.log(this.dataNikJadwal)

                    if (this.dataNikJadwal.pesan !='sukses') {
                      alert(this.dataNikJadwal.pesan)
                    } else {
                      this.cekNikJadwal = true
                    }

                    
                })
                .catch(err => {
                    // handle error
                    console.log(err);
                })
           }

          //batas method
        },
      });
    </script>

</body>

</html>