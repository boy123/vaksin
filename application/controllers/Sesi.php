<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sesi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Sesi_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sesi/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sesi/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sesi/index.html';
            $config['first_url'] = base_url() . 'sesi/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Sesi_model->total_rows($q);
        $sesi = $this->Sesi_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'sesi_data' => $sesi,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => $this->input->get('judul'),
            'konten' => 'sesi/sesi_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Sesi_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_sesi' => $row->id_sesi,
		'id_jadwal' => $row->id_jadwal,
		'kode_sesi' => $row->kode_sesi,
		'jam_mulai' => $row->jam_mulai,
		'jam_selesai' => $row->jam_selesai,
		'kategori' => $row->kategori,
		'kuota' => $row->kuota,
		'keterangan' => $row->keterangan,
	    );
            $this->load->view('sesi/sesi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sesi'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'sesi/sesi_form',
            'konten' => 'sesi/sesi_form',
            'button' => 'Create',
            'action' => site_url('sesi/create_action'),
	    'id_sesi' => set_value('id_sesi'),
	    'id_jadwal' => set_value('id_jadwal'),
	    'kode_sesi' => set_value('kode_sesi'),
	    'jam_mulai' => set_value('jam_mulai'),
	    'jam_selesai' => set_value('jam_selesai'),
	    'kategori' => set_value('kategori'),
	    'kuota' => set_value('kuota'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_jadwal' => $this->input->post('id_jadwal',TRUE),
		'kode_sesi' => $this->input->post('kode_sesi',TRUE),
		'jam_mulai' => $this->input->post('jam_mulai',TRUE),
		'jam_selesai' => $this->input->post('jam_selesai',TRUE),
		'kategori' => $this->input->post('kategori',TRUE),
		'kuota' => $this->input->post('kuota',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Sesi_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('sesi').'?'.param_get());
        }
    }
    
    public function update($id) 
    {
        $row = $this->Sesi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'sesi/sesi_form',
                'konten' => 'sesi/sesi_form',
                'button' => 'Update',
                'action' => site_url('sesi/update_action'),
		'id_sesi' => set_value('id_sesi', $row->id_sesi),
		'id_jadwal' => set_value('id_jadwal', $row->id_jadwal),
		'kode_sesi' => set_value('kode_sesi', $row->kode_sesi),
		'jam_mulai' => set_value('jam_mulai', $row->jam_mulai),
		'jam_selesai' => set_value('jam_selesai', $row->jam_selesai),
		'kategori' => set_value('kategori', $row->kategori),
		'kuota' => set_value('kuota', $row->kuota),
		'keterangan' => set_value('keterangan', $row->keterangan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sesi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_sesi', TRUE));
        } else {
            $data = array(
		'id_jadwal' => $this->input->post('id_jadwal',TRUE),
		'kode_sesi' => $this->input->post('kode_sesi',TRUE),
		'jam_mulai' => $this->input->post('jam_mulai',TRUE),
		'jam_selesai' => $this->input->post('jam_selesai',TRUE),
		'kategori' => $this->input->post('kategori',TRUE),
		'kuota' => $this->input->post('kuota',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Sesi_model->update($this->input->post('id_sesi', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('sesi').'?'.param_get());
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Sesi_model->get_by_id($id);

        if ($row) {
            $this->Sesi_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('sesi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sesi'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_jadwal', 'id jadwal', 'trim|required');
	$this->form_validation->set_rules('kode_sesi', 'kode sesi', 'trim|required');
	$this->form_validation->set_rules('jam_mulai', 'jam mulai', 'trim|required');
	$this->form_validation->set_rules('jam_selesai', 'jam selesai', 'trim|required');
	$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
	$this->form_validation->set_rules('kuota', 'kuota', 'trim|required');
	$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

	$this->form_validation->set_rules('id_sesi', 'id_sesi', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Sesi.php */
/* Location: ./application/controllers/Sesi.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-09-12 05:39:33 */
/* https://jualkoding.com */