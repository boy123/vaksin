<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jadwal_vaksin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jadwal_vaksin_model');
        $this->load->library('form_validation');
    }

    public function set($id_jadwal,$n)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->update('jadwal_vaksin', ['aktif'=>$n]);
        redirect("jadwal_vaksin");
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'jadwal_vaksin/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jadwal_vaksin/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jadwal_vaksin/index.html';
            $config['first_url'] = base_url() . 'jadwal_vaksin/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jadwal_vaksin_model->total_rows($q);
        $jadwal_vaksin = $this->Jadwal_vaksin_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jadwal_vaksin_data' => $jadwal_vaksin,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'jadwal_vaksin/jadwal_vaksin_list',
            'konten' => 'jadwal_vaksin/jadwal_vaksin_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Jadwal_vaksin_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_jadwal' => $row->id_jadwal,
		'tanggal_vaksin' => $row->tanggal_vaksin,
		'id_lokasi' => $row->id_lokasi,
		'dosis_dua' => $row->dosis_dua,
	    );
            $this->load->view('jadwal_vaksin/jadwal_vaksin_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_vaksin'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'jadwal_vaksin/jadwal_vaksin_form',
            'konten' => 'jadwal_vaksin/jadwal_vaksin_form',
            'button' => 'Create',
            'action' => site_url('jadwal_vaksin/create_action'),
	    'id_jadwal' => set_value('id_jadwal'),
	    'tanggal_vaksin' => set_value('tanggal_vaksin'),
	    'id_lokasi' => set_value('id_lokasi'),
	    'dosis_dua' => set_value('dosis_dua'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'tanggal_vaksin' => $this->input->post('tanggal_vaksin',TRUE),
		'id_lokasi' => $this->input->post('id_lokasi',TRUE),
		'dosis_dua' => $this->input->post('dosis_dua',TRUE),
	    );

            $this->Jadwal_vaksin_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jadwal_vaksin'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jadwal_vaksin_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'jadwal_vaksin/jadwal_vaksin_form',
                'konten' => 'jadwal_vaksin/jadwal_vaksin_form',
                'button' => 'Update',
                'action' => site_url('jadwal_vaksin/update_action'),
		'id_jadwal' => set_value('id_jadwal', $row->id_jadwal),
		'tanggal_vaksin' => set_value('tanggal_vaksin', $row->tanggal_vaksin),
		'id_lokasi' => set_value('id_lokasi', $row->id_lokasi),
		'dosis_dua' => set_value('dosis_dua', $row->dosis_dua),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_vaksin'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jadwal', TRUE));
        } else {
            $data = array(
		'tanggal_vaksin' => $this->input->post('tanggal_vaksin',TRUE),
		'id_lokasi' => $this->input->post('id_lokasi',TRUE),
		'dosis_dua' => $this->input->post('dosis_dua',TRUE),
	    );

            $this->Jadwal_vaksin_model->update($this->input->post('id_jadwal', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jadwal_vaksin'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jadwal_vaksin_model->get_by_id($id);

        if ($row) {
            $this->Jadwal_vaksin_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jadwal_vaksin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_vaksin'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('tanggal_vaksin', 'tanggal vaksin', 'trim|required');
	$this->form_validation->set_rules('id_lokasi', 'id lokasi', 'trim|required');
	$this->form_validation->set_rules('dosis_dua', 'dosis dua', 'trim|required');

	$this->form_validation->set_rules('id_jadwal', 'id_jadwal', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Jadwal_vaksin.php */
/* Location: ./application/controllers/Jadwal_vaksin.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-09-12 05:39:23 */
/* https://jualkoding.com */