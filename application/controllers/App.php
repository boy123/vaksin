<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Notif_model');
    }

    
	
	public function index()
	{
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function pendaftaran()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        
        $data = array(
            'konten' => 'pendaftaran/view',
            'judul_page' => 'Pendaftaran',
        );
        $this->load->view('v_index', $data);
    }

    public function cetak_rekap()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }

        if ($_POST) {
            $data['id_jadwal'] = $this->input->post('id_jadwal');
            $this->load->view('pendaftaran/cetak', $data);
        } else {
            $data = array(
                'konten' => 'pendaftaran/cetakform',
                'judul_page' => 'Cetak Pendaftaran',
            );
            $this->load->view('v_index', $data);
        }
    }

    public function cekJadwal($id_jadwal)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $sesi = $this->db->get('sesi');

        $data = [];
        foreach ($sesi->result() as $rw) {

            $kuota = $rw->kuota;

            $this->db->where('kode_sesi', $rw->kode_sesi);
            $ttl = $this->db->get('pendaftaran')->num_rows();

            $sisa_kuota = $kuota - $ttl;

            array_push($data, 
                [
                    'id_sesi' => $rw->id_sesi,
                    'kode_sesi' => $rw->kode_sesi,
                    'jam' => $rw->jam_mulai.' - '.$rw->jam_selesai,
                    'kategori' => get_data('kategori_peserta','kode',$rw->kategori,'kategori'),
                    'kuota' => $kuota,
                    'sisa_kuota' => $sisa_kuota,
                    'keterangan' => $rw->keterangan,
                ]
            );
        }
        echo json_encode($data);
    }

    public function cekNikJadwal($nik)
    {

        $data=[];

        $this->db->where('nik', $nik);
        $cek_nik = $this->db->get('pendaftaran');

        if ($cek_nik->num_rows() > 0) {
            $peserta = $cek_nik->row();

            $sesi = $this->db->get_where('sesi', ['kode_sesi'=>$peserta->kode_sesi])->row();
            $jadwal = $this->db->get_where('jadwal_vaksin', ['id_jadwal'=>$sesi->id_jadwal])->row();

            echo json_encode([
                'pesan' => 'sukses',
                'no_urut' => $peserta->no_urut,
                'nik' => $peserta->nik,
                'nama' => $peserta->nama,
                'jadwal' => $jadwal->tanggal_vaksin.' bertempat '. get_data('lokasi','id_lokasi',$jadwal->id_lokasi,'lokasi'),
                'dosis_dua' => $jadwal->dosis_dua.' bertempat '. get_data('lokasi','id_lokasi',$jadwal->id_lokasi,'lokasi'),
                'data' => [
                    array(
                        'id_sesi' => $sesi->id_sesi,
                    'kode_sesi' => $sesi->kode_sesi,
                    'jam' => $sesi->jam_mulai.' - '.$sesi->jam_selesai,
                    'kategori' => get_data('kategori_peserta','kode',$sesi->kategori,'kategori'),
                    'keterangan' => $sesi->keterangan,
                    )
                ]
            ]);

        } else {
            echo json_encode([
                'pesan' => 'Nik Tidak Ditemukan'
            ]);
        }

        
    }

    

}
