<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function index()
	{
		$this->load->view('front/index');
	}

	public function simpan_pendaftaran()
	{
		$no_urut = kode_urut($this->input->post('kode_sesi'));
		$usia = substr($this->input->post('tanggal_lahir'), 0,4) - date('Y');

		$nik = $this->input->post('nik');
		$cek_nik = $this->db->get_where('pendaftaran', ['nik'=>$nik]);
		if ($cek_nik->num_rows() > 0) {
			?>
			<script type="text/javascript">
				alert("NIK sudah terdaftar disistem, silahkan cek jadwal kamu");
				window.location = "<?php echo base_url() ?>";
			</script>
			<?php
			exit;
		}

		$this->db->insert('pendaftaran', [
			'nik' => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'kode_sesi' => $this->input->post('kode_sesi'),
			'tanggal_lahir' => $this->input->post('tanggal_lahir'),
			'usia' => $usia,
			'alamat' => $this->input->post('alamat'),
			'no_hp' => $this->input->post('no_hp'),
			'no_urut' => $no_urut
		]);

		?>
		<script type="text/javascript">
			alert("Pendaftaran Kamu Berhasil");
			window.location = "<?php echo base_url() ?>";
		</script>
		<?php

	}


}

/* End of file Web.php */
/* Location: ./application/controllers/Web.php */